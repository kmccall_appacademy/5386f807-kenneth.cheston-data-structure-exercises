# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.sort!
  (arr[0]..arr[-1]).size - 1
  # your code goes here
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr.sort == arr
  # your code goes here
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = "aeoui"
  str.downcase.chars.count {|letter| vowels.include?(letter)}
  # your code goes here
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  vowels = "aeoui"
  str = str.downcase.chars.reject {|letter| vowels.include?(letter)}
  str.join
  # your code goes here
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.sort!.reverse!
  # your code goes here
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  str.downcase.chars != str.downcase.chars.uniq
  # your code goes here
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  arr = arr.join
  "(#{arr[0..2]}) #{arr[3..5]}-#{arr[6..9]}"
  # your code goes here
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  str = str.split(",").sort

  ((str[0]).to_i..(str[-1]).to_i).size - 1
  # your code goes here
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  offset = offset % arr.size
  if offset < 0
    arr.reverse!
    offset = offset.abs
    arr = arr.drop(offset) + arr.take(offset)
    return arr.reverse
  end
  arr.drop(offset) + arr.take(offset)
end
